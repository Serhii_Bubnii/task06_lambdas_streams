package com.bubnii.view;

@FunctionalInterface
public interface Printable {

    void printResults(String printMe);

}
