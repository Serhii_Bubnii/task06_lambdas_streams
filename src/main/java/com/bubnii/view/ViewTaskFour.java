package com.bubnii.view;

import com.bubnii.App;
import com.bubnii.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ViewTaskFour implements Printable {

    private static final Logger logger = LogManager.getLogger(App.class);
    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    private Scanner input = new Scanner(System.in);
    private static Controller controller = new Controller();

    private void outMenu(int procedure) {
        switch (procedure) {
            case 0:
                System.exit(0);
            case 1:
                numberOfUniqueWords();
                break;
            case 2:
                sortedListOfUniqueWords();
                break;
            case 3:
                countUniqueWords();
                break;
            case 4:
                countNumberOfEachCharacterInText();
                break;
            default:
                printResults("Invalid number entered" + procedure);
        }
    }

    public void show() {
        int procedure;
        do {
            printResults("------------------------------------------------------");
            menu();
            printResults("Enter the menu point: ");
            procedure = input.nextInt();
            outMenu(procedure);
        } while (true);
    }

    private static void menu(){
        logger.info("Please, select menu point.");
        logger.info("Enter 1 - Calculate number of unique words: ");
        logger.info("Enter 2 - Get a sorted list of all unique words: ");
        logger.info("Enter 3 - Occurrence number of each word in the text: ");
        logger.info("Enter 4 - Occurrence number of each symbol except uppercase characters: ");
        logger.info("0 - Quit the application");
    }

    private void numberOfUniqueWords() {
        try {
            controller.getNumberOfUniqueWords(readingLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sortedListOfUniqueWords() {
        try {
            controller.getSortedListOfUniqueWords(readingLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void countUniqueWords() {
        try {
            controller.getUniqueWordsCount(readingLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void countNumberOfEachCharacterInText() {
        try {
            controller.getNumberOfEachCharacterInText(readingLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> readingLine() throws IOException {
        List<String> strings = new ArrayList<>();
        System.out.print("Enter input:");
        while (true) {
            String input = in.readLine();
            if (input.equals("")) {
                break;
            }
            strings.add(input);
        }
        return strings;
    }

    @Override
    public void printResults(String printMe) {
        logger.info(printMe);
    }

}
