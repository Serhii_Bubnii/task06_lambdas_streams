package com.bubnii.view;

import com.bubnii.App;
import com.bubnii.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ViewTaskOne implements Printable {

    private static final Logger logger = LogManager.getLogger(App.class);
    private Controller controller = new Controller();
    private Scanner input = new Scanner(System.in);;

    public void outMenu(int procedure) {
        switch (procedure) {
            case 0:
                System.exit(0);
            case 1:
                getСalculateMaxValue();
                break;
            case 2:
                getСalculateAverageValue1();
                break;
            default:
                printResults("Invalid number entered" + procedure);
        }
    }

    public void show() {
        int procedure;
        do {
            printResults("------------------------------------------------------");
            menu();
            printResults("Enter the menu point: ");
            procedure = input.nextInt();
            outMenu(procedure);
        } while (true);
    }

    private static void menu(){
        logger.info("Please, select menu point.");
        logger.info("1 - Find the maximum number");
        logger.info("2 - Find the average number");
        logger.info("0 - Quit the application");

    }

    private void getСalculateMaxValue() {
        printResults("Enter first number");
        int first = input.nextInt();
        printResults("Enter second number");
        int second = input.nextInt();
        printResults("Enter third number");
        int third = input.nextInt();
        controller.callMethodСalculateMaxValue(first, second, third);
    }

    private void getСalculateAverageValue1() {
        printResults("Enter first number");
        int first = input.nextInt();
        printResults("Enter second number");
        int second = input.nextInt();
        printResults("Enter third number");
        int third = input.nextInt();
        controller.callCalculateAverageValue1(first, second, third);
    }

    @Override
    public void printResults(String printMe) {
        logger.info(printMe);
    }
}
