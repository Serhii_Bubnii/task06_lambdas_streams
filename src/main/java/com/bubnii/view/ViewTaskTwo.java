package com.bubnii.view;

import com.bubnii.App;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ViewTaskTwo implements Printable {

    private static final Logger logger = LogManager.getLogger(App.class);

    @Override
    public void printResults(String printMe) {
        logger.info(printMe);
    }
}
