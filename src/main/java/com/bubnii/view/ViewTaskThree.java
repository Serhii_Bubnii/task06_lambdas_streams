package com.bubnii.view;

import com.bubnii.App;
import com.bubnii.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Scanner;

public class ViewTaskThree implements Printable {

    private static final Logger logger = LogManager.getLogger(App.class);
    private  Controller controller = new Controller();
    private Scanner input = new Scanner(System.in);

    public void outMenu(int procedure) {
        switch (procedure) {
            case 0:
                System.exit(0);
            case 1:
                getCountAverageMinMaxSumOfList();
                break;
            case 2:
                getCountAverageMinMaxSumOfArray();
                break;
            default:
                printResults("Invalid number entered" + procedure);
        }
    }

    public void show() {
        int procedure;
        do {
            printResults("------------------------------------------------------------");
            menu();
            printResults("Enter the menu point: ");
            procedure = input.nextInt();
            outMenu(procedure);
        } while (true);
    }

    private static void menu(){
        logger.info("Please, select menu point.");
        logger.info("1 - Create a list and count for it: average, min, max, sum");
        logger.info("2 - Create a array and count for it: average, min, max, sum");
        logger.info("0 - Quit the application");

    }

    private List<Integer> createList() {
        printResults("Enter list size");
        int size = input.nextInt();
        return controller.callCreateRandomStreamList(size);
    }

    private int[] createArray() {
        printResults("Enter array size");
        int size = input.nextInt();
        return controller.callCreateRandomArrayStream(size);
    }

    private void getCountAverageMinMaxSumOfList(){
        controller.callCountAverageMinMaxSumOfList(createList());
    }

    private void getCountAverageMinMaxSumOfArray(){
        controller.callCountAverageMinMaxSumOfArray(createArray());
    }

    @Override
    public void printResults(String printMe) {
        logger.info(printMe);
    }
}
