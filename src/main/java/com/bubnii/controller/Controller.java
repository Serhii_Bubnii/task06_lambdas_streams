package com.bubnii.controller;

import com.bubnii.model.task1.LambdaRealization;
import com.bubnii.model.task3.RandomListArray;
import com.bubnii.model.task4.TextHandler;

import java.util.List;
import java.util.Map;

public class Controller {

    private static RandomListArray randomNumber = new RandomListArray();
    private static LambdaRealization realization = new LambdaRealization();
    private TextHandler handler = new TextHandler();

    public void callMethodСalculateMaxValue(int first, int second, int third) {
        realization.calculateMaxValue(first, second, third);
    }

    public void callCalculateAverageValue1(int first, int second, int third) {
        realization.calculateAverageValue1(first, second, third);
    }

    public List<Integer> callCreateRandomList(int size) {
        return randomNumber.createRandomList(size);
    }

    public List<Integer> callCreateRandomLambdaList(int size) {
        return randomNumber.createRandomLambdaList(10);
    }

    public List<Integer> callCreateRandomStreamList(int size) {
        return randomNumber.createRandomStreamList(size);
    }

    public int[] callCreateRandomArrayLambda(int size) {
        return randomNumber.createRandomArrayLambda(size);
    }

    public int[] callCreateRandomArrayStream(int size) {
        return randomNumber.createRandomArrayStream(size);
    }

    public void callCountAverageMinMaxSumOfList(List<Integer> listInt) {
        randomNumber.countAverageMinMaxSumOfList(listInt);
    }

    public void callCountAverageMinMaxSumOfArray(int[] arrayInt) {
        randomNumber.countAverageMinMaxSumOfArray(arrayInt);
    }

    public long getNumberOfUniqueWords(List<String> list){
        return handler.countNumberOfUniqueWords(list);
    }

    public List<String> getSortedListOfUniqueWords(List<String> list){
        return handler.countSortedListOfUniqueWords(list);
    }

    public Map<String, Long> getUniqueWordsCount(List<String> list){
        return handler.countUniqueWords(list);
    }

    public Map<String, Long> getNumberOfEachCharacterInText(List<String> list){
        return handler.countNumberOfEachCharacterInText(list);
    }
}
