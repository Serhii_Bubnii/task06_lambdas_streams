package com.bubnii.model.task1;

@FunctionalInterface
public interface Functional {

    int handleNumbers(int first, int second, int third);

}
