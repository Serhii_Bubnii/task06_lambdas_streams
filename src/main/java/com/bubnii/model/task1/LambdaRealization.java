package com.bubnii.model.task1;

import com.bubnii.view.Printable;
import com.bubnii.view.ViewTaskOne;

import java.util.IntSummaryStatistics;
import java.util.stream.Stream;

public class LambdaRealization {

    private Printable view = new ViewTaskOne();

    public void calculateMaxValue(int first, int second, int third) {
        Functional maxValueTwo = (a, b, c) -> (a > b) && (a > c) ? a : b > c ? b : c;
        view.printResults("Уou have entered three numbers: [" + first + "," + second + "," + third + "]");
        view.printResults("Max value " + maxValueTwo.handleNumbers(first, second, third));
    }

    public void calculateAverageValue1(int first, int second, int third) {
        Functional averageValue1 = (a, b, c) -> {
            IntSummaryStatistics list = Stream.of(a, b, c)
                    .mapToInt(Integer::intValue)
                    .summaryStatistics();
            return (int) list.getAverage();
        };
        view.printResults("Уou have entered three numbers: [" + first + "," + second + "," + third + "]");
        view.printResults("Average value " + averageValue1.handleNumbers(first, second, third));
    }
}
