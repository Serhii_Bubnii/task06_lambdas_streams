package com.bubnii.model.task4;

import com.bubnii.view.Printable;
import com.bubnii.view.ViewTaskFour;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextHandler {

    private Printable view = new ViewTaskFour();

    public long countNumberOfUniqueWords(List<String> stringList) {
        long unique = stringList.stream().flatMap(line -> Stream.of(line.toLowerCase().split(" "))).distinct().count();
        view.printResults("Number of unique words: " + unique);
        return unique;
    }

    public List<String> countSortedListOfUniqueWords(List<String> stringList) {
        List<String> sortedList = stringList.stream()
                .flatMap(line -> Stream.of(line.toLowerCase().split(" ")))
                .distinct().sorted().collect(Collectors.toList());
        view.printResults("Sorted list of unique words: " + sortedList);
        return sortedList;
    }

    public Map<String, Long> countUniqueWords(List<String> stringList) {
        Map<String, Long> uniqueWordsCount = stringList.stream()
                .flatMap(line -> Stream.of(line.split(" ", 10)))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        view.printResults("Unique words count: " + uniqueWordsCount);
        return uniqueWordsCount;
    }

    public Map<String, Long> countNumberOfEachCharacterInText(List<String> stringList) {
        Map<String, Long> symbolsMap = stringList.stream()
                .flatMap(line -> Stream.of(line.split(" ", 2)))
                .flatMap(s -> Stream.of(s.split("")))
                .filter(s -> (Character.isLowerCase(s.charAt(0))))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        view.printResults("Unique words count: " + symbolsMap);
        return symbolsMap;
    }
}
