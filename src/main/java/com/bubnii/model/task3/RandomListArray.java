package com.bubnii.model.task3;

import com.bubnii.view.Printable;
import com.bubnii.view.ViewTaskThree;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomListArray {

    private Printable view = new ViewTaskThree();

    public List<Integer> createRandomList(int listSize) {
        List<Integer> list = new ArrayList<>(listSize);
        Random random = new Random();
        for (int i = 0; i < listSize; i++) {
            list.add(random.nextInt(100));
        }
        return list;
    }

    public List<Integer> createRandomLambdaList(int listSize) {
        List<Integer> list = new Random()
                .ints(0, 100)
                .limit(listSize)
                .boxed()
                .collect(Collectors.toList());
        return list;
    }

    public List<Integer> createRandomStreamList(int listSize) {
        List<Integer> list =ThreadLocalRandom
                .current()
                .ints(listSize, 1, 100)
                .boxed()
                .collect(Collectors.toList());
        return list;
    }


    public int[] createRandomArrayStream(int arraySize) {
        int[] arr = IntStream.generate(() -> new Random()
                .nextInt(100))
                .limit(arraySize)
                .toArray();
        return arr;
    }

    public int[] createRandomArrayLambda(int arraySize) {
        int[] arr =new Random()
                .ints(arraySize, 0, 100)
                .toArray();
        return arr;
    }

    public void countAverageMinMaxSumOfList(List<Integer> list) {
        Optional<Integer> maxNumber = list.stream().max(Comparator.naturalOrder());
        Optional<Integer> minNumber = list.stream().min(Comparator.naturalOrder());
        double averageNumber = list.stream().mapToDouble(Integer::intValue).average().orElse(Double.NaN);
        int sumOne = list.stream().reduce(0, Integer::sum);
        long bigAverageNumber = list.stream().filter(o -> o > averageNumber).distinct().count();
        view.printResults("List: " + list);
        view.printResults("Average number of list: " + averageNumber);
        view.printResults("Max number: " + maxNumber.get());
        view.printResults("Min number: " + minNumber.get());
        view.printResults("Sum numbers: " + sumOne);
        view.printResults("Count number of values that are bigger than average: " + bigAverageNumber);
    }

    public void countAverageMinMaxSumOfArray(int[] array) {
        int maxNumber = Arrays.stream(array).max().getAsInt();
        double averageNumber = Arrays.stream(array).average().orElse(Double.NaN);
        int minNumber = Arrays.stream(array).min().getAsInt();
        int sumOne = Arrays.stream(array).reduce(0, Integer::sum);
        long bigAverageNumber = Arrays.stream(array).filter(o -> o > averageNumber).distinct().count();
        view.printResults("Array: " + Arrays.toString(array));
        view.printResults("Average number of list: " + averageNumber);
        view.printResults("Max number: " + maxNumber);
        view.printResults("Min number: " + minNumber);
        view.printResults("Sum numbers: " + sumOne);
        view.printResults("Count number of values that are bigger than average: " + bigAverageNumber);
    }
}
