package com.bubnii.model.task2;

public class Switch {

    public String challengeCommandLambdaFunction(String str) {
        return "Challenge command lambda function :" + str;
    }

    public String challengeCommandAnonymousClass(String str) {
        return "Challenge command anonymous class : " + str;
    }

    public String challengeCommandSimpleClass(String str) {
        return "Challenge command simple class : " + str;
    }

    public String challengeCommandMethodReference(String str) {
        return "Challenge command method reference : " + str;
    }

}
