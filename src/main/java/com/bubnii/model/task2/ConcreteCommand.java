package com.bubnii.model.task2;

public class ConcreteCommand {
    private Switch aSwitch = new Switch();

    public Command commandAnonymousClass() {
        return new Command() {
            @Override
            public String execute(String str) {
                return aSwitch.challengeCommandAnonymousClass(str);
            }
        };
    }

    public Command commandLambdaFunction(){
        return str -> aSwitch.challengeCommandLambdaFunction(str);
    }

    public Command commandMethodReference(){
        return (aSwitch::challengeCommandMethodReference);
    }
}
