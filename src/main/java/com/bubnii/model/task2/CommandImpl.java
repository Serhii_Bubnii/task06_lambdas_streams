package com.bubnii.model.task2;

public class CommandImpl implements Command {

    private Switch aSwitch = new Switch();

    @Override
    public String execute(String str) {
       return aSwitch.challengeCommandSimpleClass(str);
    }
}
